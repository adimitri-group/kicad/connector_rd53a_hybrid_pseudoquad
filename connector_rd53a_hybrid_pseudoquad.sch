EESchema Schematic File Version 4
LIBS:connector_rd53a_hybrid_pseudoquad-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Single Chip Connector to DisplayPort"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L connsingle:ConnSingle J_conn1
U 1 1 5BE2176A
P 2150 3450
F 0 "J_conn1" H 2250 5850 60  0000 C CNN
F 1 "ConnSingle" H 2200 2250 60  0000 C CNN
F 2 "Library:connector" H 2150 2950 60  0001 C CNN
F 3 "" H 2150 2950 60  0001 C CNN
	1    2150 3450
	1    0    0    -1  
$EndComp
$Comp
L display_port:DISPLAY_PORT J1
U 1 1 5BE217B8
P 2500 6050
F 0 "J1" H 1900 7150 60  0000 C CNN
F 1 "DISPLAY_PORT" V 2650 6050 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 2450 6050 60  0001 C CNN
F 3 "" H 2450 6050 60  0000 C CNN
	1    2500 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5BE21828
P 9500 1500
F 0 "C1" H 9525 1600 50  0000 L CNN
F 1 "100n" H 9525 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 9538 1350 50  0001 C CNN
F 3 "" H 9500 1500 50  0001 C CNN
	1    9500 1500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5BE21869
P 9850 1500
F 0 "C2" H 9875 1600 50  0000 L CNN
F 1 "100n" H 9875 1400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402_NoSilk" H 9888 1350 50  0001 C CNN
F 3 "" H 9850 1500 50  0001 C CNN
	1    9850 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5BE218B0
P 9600 2100
F 0 "#PWR01" H 9600 1850 50  0001 C CNN
F 1 "GND" H 9600 1950 50  0000 C CNN
F 2 "" H 9600 2100 50  0001 C CNN
F 3 "" H 9600 2100 50  0001 C CNN
	1    9600 2100
	1    0    0    -1  
$EndComp
NoConn ~ 1600 1450
NoConn ~ 2900 1350
NoConn ~ 2900 1750
NoConn ~ 2900 1950
NoConn ~ 2900 2150
NoConn ~ 2900 2350
NoConn ~ 1600 2450
NoConn ~ 1600 2250
NoConn ~ 1600 2050
NoConn ~ 1600 1850
NoConn ~ 1650 6850
NoConn ~ 1650 6950
NoConn ~ 1650 7050
Wire Wire Line
	2900 1550 3550 1550
Wire Wire Line
	2900 2550 3550 2550
Wire Wire Line
	2900 2750 3550 2750
Wire Wire Line
	2900 2950 3550 2950
Wire Wire Line
	2900 3150 3550 3150
Wire Wire Line
	2900 3350 3550 3350
Wire Wire Line
	2900 3550 3550 3550
Wire Wire Line
	2900 3750 3550 3750
Wire Wire Line
	2900 3950 3550 3950
Wire Wire Line
	2900 4150 3550 4150
Wire Wire Line
	2900 4350 3550 4350
Wire Wire Line
	1600 1250 1000 1250
Wire Wire Line
	1600 2650 1000 2650
Wire Wire Line
	1600 2850 1000 2850
Wire Wire Line
	1600 3050 1000 3050
Wire Wire Line
	1600 3250 1000 3250
Wire Wire Line
	1600 3450 1000 3450
Wire Wire Line
	1600 3650 1000 3650
Wire Wire Line
	1600 3850 1000 3850
Wire Wire Line
	1600 4050 1000 4050
Wire Wire Line
	1600 4250 1000 4250
Wire Wire Line
	1600 4450 1000 4450
Wire Wire Line
	1650 5150 1150 5150
Wire Wire Line
	1650 5250 1150 5250
Wire Wire Line
	1650 5350 1150 5350
Wire Wire Line
	1650 5450 1150 5450
Wire Wire Line
	1650 5550 1150 5550
Wire Wire Line
	1650 5650 1150 5650
Wire Wire Line
	1650 5750 1150 5750
Wire Wire Line
	1650 5850 1150 5850
Wire Wire Line
	1650 5950 1150 5950
Wire Wire Line
	1650 6050 1150 6050
Wire Wire Line
	1650 6150 1150 6150
Wire Wire Line
	1650 6250 1150 6250
Wire Wire Line
	1650 6350 1150 6350
Wire Wire Line
	1650 6450 1150 6450
Wire Wire Line
	1650 6550 1150 6550
Wire Wire Line
	1650 6650 1150 6650
Wire Wire Line
	1650 6750 1150 6750
Text Label 1000 1250 0    60   ~ 0
FE_HV
Text Label 1000 2650 0    60   ~ 0
FE_NTC_RET
Text Label 1000 2850 0    60   ~ 0
FE_MUX
Text Label 1000 3050 0    60   ~ 0
GND
Text Label 1000 3250 0    60   ~ 0
FE_DO_0_P
Text Label 1000 3450 0    60   ~ 0
FE_DO_1_P
Text Label 1000 3650 0    60   ~ 0
GND
Text Label 1000 3850 0    60   ~ 0
FE_DO_2_P
Text Label 1000 4050 0    60   ~ 0
FE_DO_3_P
Text Label 1000 4250 0    60   ~ 0
GND
Text Label 1000 4450 0    60   ~ 0
FE_CMD_P
Wire Wire Line
	1600 1650 1000 1650
Text Label 1000 1650 0    60   ~ 0
GND
Text Label 3550 1550 2    60   ~ 0
FE_HV_RET
Text Label 3550 2550 2    60   ~ 0
GND
Text Label 3550 2750 2    60   ~ 0
FE_NTC
Text Label 3550 2950 2    60   ~ 0
FE_VIN
Text Label 3550 3150 2    60   ~ 0
FE_DO_0_N
Text Label 3550 3350 2    60   ~ 0
GND
Text Label 3550 3550 2    60   ~ 0
FE_DO_1_N
Text Label 3550 3750 2    60   ~ 0
FE_DO_2_N
Text Label 3550 3950 2    60   ~ 0
GND
Text Label 3550 4150 2    60   ~ 0
FE_DO_3_N
Text Label 3550 4350 2    60   ~ 0
FE_CMD_N
Text Label 1150 5150 0    60   ~ 0
FE_DO_0_N
Text Label 1150 5250 0    60   ~ 0
FGND
Text Label 1150 5350 0    60   ~ 0
FE_DO_0_P
Text Label 1150 5450 0    60   ~ 0
FE_DO_1_N
Text Label 1150 5650 0    60   ~ 0
FE_DO_1_P
Text Label 1150 5750 0    60   ~ 0
FE_DO_2_N
Text Label 1150 5950 0    60   ~ 0
FE_DO_2_P
Text Label 1150 6050 0    60   ~ 0
FE_DO_3_N
Text Label 1150 6250 0    60   ~ 0
FE_DO_3_P
Text Label 1150 5550 0    60   ~ 0
FGND
Text Label 1150 5850 0    60   ~ 0
FGND
Text Label 1150 6150 0    60   ~ 0
FGND
Text Label 1150 6350 0    60   ~ 0
FE_NTC
Text Label 1150 6450 0    60   ~ 0
FE_NTC_RET
Text Label 1150 6550 0    60   ~ 0
FE_CMD_N
Text Label 1150 6750 0    60   ~ 0
FE_CMD_P
Text Label 1150 6650 0    60   ~ 0
FGND
Text Label 2300 7400 0    60   ~ 0
SGND
Text Label 9600 2100 0    60   ~ 0
GND
Text Label 9500 1650 3    60   ~ 0
GND
Text Label 9850 1650 3    60   ~ 0
GND
Text Label 9850 1350 0    60   ~ 0
SGND
Text Label 9500 1350 0    60   ~ 0
FGND
$Comp
L molex:molex M1
U 1 1 5BE216A4
P 10050 3050
F 0 "M1" H 10040 3220 60  0000 C CNN
F 1 "molex" H 10049 2885 60  0000 C CNN
F 2 "Library:molex" H 10350 3400 60  0001 C CNN
F 3 "" H 10350 3400 60  0001 C CNN
	1    10050 3050
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U3
U 1 1 5BE2171C
P 10650 3700
F 0 "U3" H 10700 3850 59  0000 C CNN
F 1 "Solder_Pad" H 10700 3950 59  0000 C CNN
F 2 "Library:Power_Pad" H 11500 4100 59  0001 C CNN
F 3 "" H 11500 4100 59  0001 C CNN
	1    10650 3700
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U4
U 1 1 5BE21773
P 10650 4100
F 0 "U4" H 10700 4250 59  0000 C CNN
F 1 "Solder_Pad" H 10700 4350 59  0000 C CNN
F 2 "Library:Power_Pad" H 11500 4500 59  0001 C CNN
F 3 "" H 11500 4500 59  0001 C CNN
	1    10650 4100
	1    0    0    -1  
$EndComp
$Comp
L solder_pad:Solder_Pad U1
U 1 1 5BE217E2
P 9650 3700
F 0 "U1" H 9700 3850 59  0000 C CNN
F 1 "Solder_Pad" H 9700 3950 59  0000 C CNN
F 2 "Library:Power_Pad" H 10500 4100 59  0001 C CNN
F 3 "" H 10500 4100 59  0001 C CNN
	1    9650 3700
	-1   0    0    1   
$EndComp
$Comp
L solder_pad:Solder_Pad U2
U 1 1 5BE2188B
P 9650 4100
F 0 "U2" H 9700 4250 59  0000 C CNN
F 1 "Solder_Pad" H 9700 4350 59  0000 C CNN
F 2 "Library:Power_Pad" H 10500 4500 59  0001 C CNN
F 3 "" H 10500 4500 59  0001 C CNN
	1    9650 4100
	-1   0    0    1   
$EndComp
$Comp
L pinheader_1x4:PinHeader_1x4 PH1
U 1 1 5BE21957
P 5600 1800
F 0 "PH1" H 5600 2300 60  0000 C CNN
F 1 "PinHeader_1x4" H 5600 1450 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5600 1800 60  0001 C CNN
F 3 "" H 5600 1800 60  0001 C CNN
	1    5600 1800
	1    0    0    -1  
$EndComp
$Comp
L pinheader_1x4:PinHeader_1x4 PH2
U 1 1 5BE219CD
P 5600 2800
F 0 "PH2" H 5600 3300 60  0000 C CNN
F 1 "PinHeader_1x4" H 5600 2450 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5600 2800 60  0001 C CNN
F 3 "" H 5600 2800 60  0001 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
$Comp
L pinheader_1x4:PinHeader_1x4 PH3
U 1 1 5BE21A15
P 5600 3800
F 0 "PH3" H 5600 4300 60  0000 C CNN
F 1 "PinHeader_1x4" H 5600 3450 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5600 3800 60  0001 C CNN
F 3 "" H 5600 3800 60  0001 C CNN
	1    5600 3800
	1    0    0    -1  
$EndComp
$Comp
L pinheader_1x4:PinHeader_1x4 PH4
U 1 1 5BE21AC4
P 5600 4800
F 0 "PH4" H 5600 5300 60  0000 C CNN
F 1 "PinHeader_1x4" H 5600 4450 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5600 4800 60  0001 C CNN
F 3 "" H 5600 4800 60  0001 C CNN
	1    5600 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4500 4600 4500
Wire Wire Line
	5200 4650 4600 4650
Text Label 4600 4500 0    60   ~ 0
FE_MUX
Text Label 4600 4650 0    60   ~ 0
GND
Wire Wire Line
	5200 1500 4600 1500
Text Label 4600 1500 0    60   ~ 0
FE_CMD_P
Wire Wire Line
	5200 1650 4600 1650
Text Label 4600 1650 0    60   ~ 0
FE_CMD_N
Wire Wire Line
	5200 1800 4600 1800
Text Label 4600 1800 0    60   ~ 0
FE_NTC_RET
Wire Wire Line
	5200 1950 4600 1950
Text Label 4600 1950 0    60   ~ 0
FE_NTC
Wire Wire Line
	5200 2500 4600 2500
Text Label 4600 2500 0    60   ~ 0
FE_DO_3_P
Wire Wire Line
	5200 2650 4600 2650
Text Label 4600 2650 0    60   ~ 0
FE_DO_3_N
Wire Wire Line
	5200 2800 4600 2800
Text Label 4600 2800 0    60   ~ 0
FE_DO_2_P
Wire Wire Line
	5200 2950 4600 2950
Text Label 4600 2950 0    60   ~ 0
FE_DO_2_N
Wire Wire Line
	5200 3500 4600 3500
Text Label 4600 3500 0    60   ~ 0
FE_DO_1_P
Wire Wire Line
	5200 3650 4600 3650
Text Label 4600 3650 0    60   ~ 0
FE_DO_1_N
Wire Wire Line
	5200 3800 4600 3800
Text Label 4600 3800 0    60   ~ 0
FE_DO_0_P
Wire Wire Line
	5200 3950 4600 3950
Text Label 4600 3950 0    60   ~ 0
FE_DO_0_N
Wire Wire Line
	5200 4800 4600 4800
Wire Wire Line
	5200 4950 4600 4950
Text Label 4600 4800 0    60   ~ 0
FE_VIN
Text Label 4600 4950 0    60   ~ 0
GND
Text Label 10350 3000 0    60   ~ 0
GND
Text Label 10350 3100 0    60   ~ 0
GND
Text Label 9750 3000 2    60   ~ 0
VIN_RAW
Text Label 9750 3100 2    60   ~ 0
VIN_RAW
Text Label 10450 3700 2    60   ~ 0
GND
Text Label 10450 4100 2    60   ~ 0
GND
Text Label 9850 3700 0    60   ~ 0
VIN_RAW
Text Label 9850 4100 0    60   ~ 0
VIN_RAW
$Comp
L Connector1:LEMO2 J2
U 1 1 5BE21E4B
P 10050 5200
F 0 "J2" H 10050 5450 50  0000 C CNN
F 1 "LEMO2" H 10050 5050 50  0000 C CNN
F 2 "Library:lemo_epb00250ntn" H 10050 5250 50  0001 C CNN
F 3 "" H 10050 5250 50  0001 C CNN
	1    10050 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 5100 9150 5100
Text Label 9150 5100 0    60   ~ 0
FE_HV
Wire Wire Line
	9750 5200 9150 5200
Text Label 9150 5200 0    60   ~ 0
FE_HV_RET
$EndSCHEMATC
